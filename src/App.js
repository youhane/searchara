import * as tf from '@tensorflow/tfjs'
import { useEffect, useState } from 'react'
import { TARGET_CLASSES } from './target_classes';

function App() {
  const [model, setModel] = useState();
  var selectedImage = ''

  const getChara = (e) => {
    let reader = new FileReader();
    reader.onload = () => {
      let dataURL = reader.result
      selectedImage = document.getElementById('selectedImg')
      selectedImage.setAttribute('src', dataURL)
    }
    reader.readAsDataURL(e.target.files[0])
  }
  
  const predictChara = async () => {
    let tensor = tf.browser.fromPixels(selectedImage, 3)
    .resizeNearestNeighbor([96,96])
    .expandDims(0)
    .div(tf.scalar(255.0))
    let predictions = await model.predict(tensor).data
    let res = predictions.map((p, i) => {
      return {
        probability: p*10,
        className: TARGET_CLASSES[i]
      }
    }).sort((a, b) => {
      return b.probability - a.probability
    }).slice(0,5)
  }

  const loadModel = async () => {
    try {
      const model = await tf.loadLayersModel('/model/model.json')
      console.log(model)
      setModel(model)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    tf.ready().then(() => {
      loadModel()
    })
  }, []);

  return (
    <div className="App">
      <h1>Searchara</h1>
      <input type="file" name="pic" onChange={(e) => getChara(e)}/>
      <button onClick={() => predictChara()}>Predict</button>
      <img id='selectedImg' />
    </div>
  );
}

export default App;
